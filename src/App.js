import React, { useState, useEffect } from "react";
import "./App.css";
import AppHeader from "./AppHeader";
import Table from "./Table";
import { AddEmp } from "./AddEmp";
import Config from "./config";
import Service from "./service";

const App = () => {
  const [employeList, setEmployeList] = useState([]);

  useEffect(() => {
    getEmployee();
  }, []);

  const getEmployee = () => {
    //Example using fetch
    Service.getAllEmployee().then((data) => {
      console.log("response data---------------", data);
      setEmployeList(data);
    });

    Service.healthCheck().then((data) => {
      console.log("response health Chekck---------------", data);
    });
  };

  const addEmp = (emp) => {
    console.log("emp------------", emp);
    setEmployeList([...employeList, emp]);
  };

  const deleteEmp = (ind) => {
    let temEmp = [...employeList];
    temEmp.splice(ind, 1);
    setEmployeList([...temEmp]);
  };

  return (
    <div className="app">
      <AppHeader />
      <div className="content">
        <AddEmp onAddEmp={addEmp}></AddEmp>
        <Table data={employeList} deleteEmp={deleteEmp} />
      </div>
    </div>
  );
};

export default App;
