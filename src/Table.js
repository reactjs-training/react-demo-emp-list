import React from "react";

const Table = ({ data = [], deleteEmp = () => {} }) => {
  return (
    <div className="table-container section">
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>DOB</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Blood gp</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {data.map(({ name, dob, addrs, phone, blgp }, ind) => (
            <tr key={`emp-${ind}`}>
              <td>{name || "--"}</td>
              <td>{dob || "--"}</td>
              <td>{addrs || "--"}</td>
              <td>{phone || "--"}</td>
              <td>{blgp || "--"}</td>
              <td className="actions" onClick={(e) => deleteEmp(ind)}>
                Delete
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
