const jsonServer = require("json-server");
const server = jsonServer.create();
const path = require("path");
const router = jsonServer.router(path.join(__dirname, "db.json")),
  db = router.db;
const middlewares = jsonServer.defaults();
const cors = require("cors");

server.use(cors());
server.use(middlewares);
server.use(jsonServer.bodyParser);


server.get("/api/getEmployee", (req, res) => {
  res.json(db.get("employee").value())
})

// server.use(router);
server.use("/api/", router);
server.listen(3001, () => {
  console.log("JSON Server is running-------------------------------");
});
