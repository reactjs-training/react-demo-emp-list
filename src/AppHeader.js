import React, { useState, useEffect } from "react";
import Logo from "./images/logo.png";

const Clock = () => {
  const [date, setDate] = useState(new Date().toLocaleTimeString());
  
  useEffect(() => {
    console.log("I am in useEffect for componentDidMount");
    setInterval(() => {
      updateTime();
    }, 1000);
  }, []); //componentDidMount

  const updateTime = () => {
    setDate(new Date().toLocaleTimeString());
  };
  return <h2>{date}</h2>;
};

const AppHeader = () => {
  return (
    <div className="app-header">
      <img src={Logo}  alt="img"></img>
      <h1 className="app-title">React App</h1>
      <Clock />
    </div>
  );
}

export default AppHeader;
