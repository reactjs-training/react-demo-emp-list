import React, { useState } from "react";

const useFormInput = ({ initialVal = "" }) => {
  const [value, setValue] = useState(initialVal);
  const handleOnChange = (e) => {
    const val = e.target.value;
    setValue(val);
  };

  return {
    value,
    onChange: handleOnChange,
  };
};

const EmpData = ({ onAddEmp = () => {} }) => {
  // const name = useFormInput(""),
  const dob = useFormInput(""),
    addrs = useFormInput(""),
    phone = useFormInput(""),
    blgp = useFormInput("");

  const [name, setName] = useState("");

  console.log("name----------", name);
  const addEmp = () => {
    onAddEmp({
      name,
      dob: dob.value,
      addrs: addrs.value,
      phone: phone.value,
      blgp: blgp.value,
    });
  };
  return (
    <React.Fragment>
      <div className="emp-input">
        <input
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        ></input>
        <input placeholder="dob" {...dob}></input>
        <input placeholder="addrs" {...addrs}></input>
        <input placeholder="phone" {...phone}></input>
        <input placeholder="blgp" {...blgp}></input>
      </div>
      <button
        onClick={addEmp}
        style={{ alignSelf: "flex-end" }}
        className="btn"
      >
        OK
      </button>
    </React.Fragment>
  );
};

const AddEmp = ({ onAddEmp = () => {} }) => {
  const [toggle, setToggle] = useState(false);

  const onToggle = () => {
    setToggle(!toggle);
  };

  return (
    <div className="section">
      <button
        style={{ alignSelf: "flex-start" }}
        className="btn"
        onClick={onToggle}
      >
        +
      </button>
      {toggle && (
        <EmpData
          onAddEmp={(emp) => {
            onAddEmp(emp);
            setToggle(false);
          }}
        />
      )}
    </div>
  );
};

export { AddEmp };
