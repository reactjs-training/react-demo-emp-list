import Config from "./config";

export default {
  getAllEmployee: () => {
    return fetch(`${Config.apiUrl}/getEmployee`).then((res) => res.json());
  },
  healthCheck: () => {
    return fetch(`${Config.apiUrl}/health`).then((res) => res.json());
  }
}
